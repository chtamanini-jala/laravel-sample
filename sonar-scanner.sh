YOUR_REPO = "https://gitlab.com/chtamanini_jala/mavengitlabproject"

docker run \
    --rm \
    -e SONAR_HOST_URL="http://${jdbc:postgresql://db:5432/sonar}" \
    -e SONAR_LOGIN="myAuthenticationToken" \
    -v "${YOUR_REPO}:/usr/src" \
    sonarsource/sonar-scanner-cli